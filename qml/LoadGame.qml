import QtQuick 2.6
import QtQuick.Controls 1.3

Item {
    id: loadGameScreen
    Item {
        Repeater {
            model: logic

            Component {
                Image {
                    height: squareSize
                    width : squareSize

                    x: squareSize * positionX
                    y: squareSize * positionY

                    source: images[color][type].imgPath
                }
            }
        }
        Column {
            spacing: 10
            x: gameBoard.width + 10
            Button {
                id: nextStep

                width: 200
                height: 40
                text: qsTr("Next")
                onClicked: {
                    logic.nextStep();
                }
            }
            Button {
                id: prevStep

                width: 200
                height: 40
                text: qsTr("Prev")
                onClicked: {
                    logic.prevStep();
                }
            }
            Button {
                id: startGame

                width: 200
                height: 40
                text: qsTr("End Game")
                onClicked: {
                    logic.clear();
                    loader.setSource("StartScreen.qml");
                }
            }
            TextEdit {
                id: turns
                width: contentWidth
                text: logic.playersTurn ? "WHITE turns" : "BLACK turns"
            }
        }
    }
}
