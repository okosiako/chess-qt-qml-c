import QtQuick 2.6
import QtQuick.Controls 1.3
import QtQuick.Dialogs 1.2

Item {
    Rectangle {
        width: windowWidth
        height: windowHeight
        color: "grey"
        opacity: 0.3
    }
    Column {
        padding: windowHeight / 3
        spacing: 10
        Button {
            id: startGame

            width: 200
            height: 40
            text: qsTr("Start Game")
            onClicked: {
                logic.startGame();
                loader.setSource("NewGame.qml");

            }
        }
        Button {
            id: loadGame

            width: 200
            height: 40
            text: qsTr("Load Game")
            onClicked: {
                //fileDialog.open();
				logic.startGame();
                logic.loadGame();
                loader.setSource("LoadGame.qml");
            }
        }
        FileDialog {
            id: fileDialog
        }
    }
    /*FileDialog {
        id: fileDialog
        visible: fileDialogVisible.checked
        title: "Please choose a file"
        folder: shortcuts.home
        onAccepted: {
            console.log("You chose: " + fileDialog.fileUrls)
           // Qt.quit()
        }
        onRejected: {
            console.log("Canceled")
            //Qt.quit()
        }
        //Component.onCompleted: visible = true
    }*/
}
