import QtQuick 2.6
//import QtQuick.Controls 1.3
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1

import QtQuick.Window 2.2

ApplicationWindow {
    title: qsTr("Chess")
    visible: true
    width: 800
    height: 600
    id: root

    property int squareSize: 70

    property int windowWidth: root.width
    property int windowHeight: root.height

    property var images: [
        [
            {'imgPath' : "/images/white_pawn.svg"},
            {'imgPath' : "/images/white_rook.svg"},
            {'imgPath' : "/images/white_knight.svg"},
            {'imgPath' : "/images/white_bishop.svg"},
            {'imgPath' : "/images/white_king.svg"},
            {'imgPath' : "/images/white_queen.svg"}],
        [
            {'imgPath' : "/images/black_pawn.svg"},
            {'imgPath' : "/images/black_rook.svg"},
            {'imgPath' : "/images/black_knight.svg"},
            {'imgPath' : "/images/black_bishop.svg"},
            {'imgPath' : "/images/black_king.svg"},
            {'imgPath' : "/images/black_queen.svg"}]
    ]

    Item {
        id: gameBoard
        x: 0
        y: 0
        width : logic.boardSize * squareSize
        height: logic.boardSize * squareSize

        Image {
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            source: "/images/chess_board.jpg"
            anchors.fill: parent
            z: -1
            fillMode: Image.Stretch
        }

        Loader {
            id: loader
            focus: true

            source: "StartScreen.qml"
        }
    }
}
