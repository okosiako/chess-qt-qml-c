import QtQuick 2.6
import QtQuick.Controls 1.3

Item {
        id: newGameScreen
        Item {

            Repeater {
                model: logic
                Component {
                    Image {
                        height: squareSize
                        width : squareSize

                        x: squareSize * positionX
                        y: squareSize * positionY

                        source: images[color][type].imgPath

                        MouseArea {
                            anchors.fill: parent
                            drag.target: parent

                            property int startX: 0
                            property int startY: 0

                            onPressed: {
                                startX = parent.x;
                                startY = parent.y;
                                parent.height = parent.width *= 1.2;
                            }

                            onReleased: {
                                var fromX = startX / squareSize;
                                var fromY = startY / squareSize;
                                var toX   = (parent.x + mouseX) / squareSize;
                                var toY   = (parent.y + mouseY) / squareSize;

                                if (logic.move(fromX, fromY, toX, toY))
                                {
                                    parent.x = Math.floor(toX) * 70;
                                    parent.y = Math.floor(toY) * 70;
                                }
                                else
                                {
                                    parent.x = startX;
                                    parent.y = startY;
                                }
                                parent.height = parent.width /= 1.2;
                            }
                        }

                    }
                }
            }
            Column {
                id: sideBar
                spacing: 10
                x: gameBoard.width + 10
                Button {
                    id: stopGame

                    width: 200
                    height: 40
                    text: qsTr("End Game")
                    onClicked: {
                        if (logic.endGame())
                            loader.setSource("StartScreen.qml");
                    }
                }
                Button {
                    id: saveGame

                    width: 200
                    height: 40
                    text: qsTr("Save Game")
                    onClicked: {
                        logic.saveGame();
                    }
                }
                TextEdit {
                    id: turns
                    width: contentWidth
                    text: logic.playersTurn ? "WHITE turns" : "BLACK turns"
                }
                TextEdit {
                    id: history
                    width: 200
                    text: logic.moveString
                }
            }
        }
    }
