#ifndef IMPLSTRUCT_H
#define IMPLSTRUCT_H

#include <QList>
#include "newtypes.h"

struct Figure
{
    int type;
    int color;
    int x;
    int y;
};

struct Impl
{
    QList<Figure>   figures;

    int             findByPosition(int x, int y);
    bool            canMakeMove(const int curr_figure, const int toX, const int toY);

private:
    int             chooseFunc (int index, int toX, int toY);
    int             knightMove(int index, int toX, int toY);
    int             bishopMove(int index, int toX, int toY);
    int             pawnMove(int index, int toX, int toY);
    int             kingMove(int index, int toX, int toY);
    int             rookMove(int index, int toX, int toY);
    int             queenMove(int index, int toX, int toY);

    bool            isOutOfRange(const int toX, int toY);
    bool            haveOneColor(const int curr_figure, const int target_cell);
    int             noBars(const int index, const int stepX, const int stepY, const int toX, const int toY);
};

#endif // IMPLSTRUCT_H
