#include "implstruct.h"

int Impl::chooseFunc(int index, int toX, int toY)
{
    int type_id = figures[index].type;

    switch (type_id) {
    case pawn   : return (pawnMove(index, toX, toY));
    case rook   : return (rookMove(index, toX, toY));
    case knight : return (knightMove(index, toX, toY));
    case bishop : return (bishopMove(index, toX, toY));
    case king   : return (kingMove(index, toX, toY));
    case queen  : return (queenMove(index, toX, toY));
    default     : return false;
    }
}

bool Impl::haveOneColor(const int curr_figure, const int target_cell)
{
    if (target_cell >= 0)
        if (figures[curr_figure].color == figures[target_cell].color)
            return true;
    return false;
}

bool Impl::isOutOfRange(const int toX, int toY)
{
    return (toX < 0 || toX >= BOARD_SIZE || toY < 0 || toY >= BOARD_SIZE);
}

bool Impl::canMakeMove(const int curr_figure, const int toX, const int toY)
{
    const int target_cell = findByPosition(toX, toY);
    if (curr_figure < 0 ||
            isOutOfRange(toX, toY) ||
            haveOneColor(curr_figure, target_cell) ||
            chooseFunc(curr_figure, toX, toY))
        return false;
    return true;
}

int Impl::knightMove(int index, int toX, int toY)
{
    const int deltaX = abs(figures[index].x - toX);
    const int deltaY = abs(figures[index].y - toY);

    if ((deltaX == 1 && deltaY == 2) || (deltaX == 2 && deltaY == 1))
        return false;
    return true;
}

int Impl::bishopMove(int index, int toX, int toY)
{
    const int deltaX = abs(figures[index].x - toX);
    const int deltaY = abs(figures[index].y - toY);

    if (deltaX != deltaY)
        return true;

    int stepX = figures[index].x < toX ? 1 : -1;
    int stepY = figures[index].y < toY ? 1 : -1;
    return (noBars(index, stepX, stepY, toX, toY));
}

int Impl::pawnMove(int index, int toX, int toY)
{
    const int target_cell = findByPosition(toX, toY);
    int colorDepandes = figures[index].color == white ? -1 : 1;
    int deltaX = (figures[index].x - toX) * colorDepandes;
    int deltaY = (figures[index].y - toY) * colorDepandes;

    if (target_cell >= 0)
    {
        if (abs(deltaX) == 1 && deltaY == 1)
            return false;
    }
    int first = figures[index].color == white ? 1 : 6;
    if (deltaX < 0 || deltaY < 0 || !(deltaY == 1 || (first == figures[index].y && deltaY == 2)))
        return true;
    int stepY = colorDepandes * -1;
    return (noBars(index, 0, stepY, toX, toY));
}

int Impl::kingMove(int index, int toX, int toY)
{
    const int deltaX = abs(figures[index].x - toX);
    const int deltaY = abs(figures[index].y - toY);
    if (deltaX <= 1 && deltaY <= 1)
        return false;
    return true;
}

int Impl::rookMove(int index, int toX, int toY)
{
    const int deltaX = abs(figures[index].x - toX);
    const int deltaY = abs(figures[index].y - toY);

    if (!(deltaX != 0 || deltaY != 0))
        return true;

    bool vert = figures[index].x == toX ? true : false;
    int stepX = figures[index].x < toX ? 1 : vert ? 0 : -1;
    int stepY = figures[index].y < toY ? 1 : vert ? -1 : 0;
    return (noBars(index, stepX, stepY, toX, toY));
}

int Impl::noBars(const int index, const int stepX, const int stepY, const int toX, const int toY)
{
    int x = figures[index].x;
    int y = figures[index].y;
    int type = figures[index].type;
    do {
        x += stepX;
        y += stepY;
    } while (findByPosition(x, y) == -1 && (type == bishop ? (x != toX && y != toY) : stepX == 0 || type == pawn ? y != toY : x != toX ));
    if (x == toX && y == toY)
    {
        if (type == pawn && findByPosition(x, y) >= 0)
            return true;
        return false;
    }
    return true;
}

int Impl::queenMove(int index, int toX, int toY)
{
    if (!(rookMove(index, toX, toY)) || !(bishopMove(index, toX, toY)))
        return false;
    return true;
}

int Impl::findByPosition(int x, int y) {
    for (int i = 0; i < figures.size(); ++i) {
        if (figures[i].x != x || figures[i].y != y ) {
            continue;
        }
        return i;
    }
    return -1;
}
