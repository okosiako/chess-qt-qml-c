#ifndef MESSAGES_H
#define MESSAGES_H

#include <QDialog>
#include "newtypes.h"
#include <QMessageBox>

class Messages
{
public:
    Messages();
    ~Messages();

    bool createMessage(const int type);
};

#endif // MESSAGES_H
