#ifndef NEWTYPES_H
#define NEWTYPES_H

enum GlobalConstants {
    BOARD_SIZE = 8
};

enum figures_type {
    pawn,
    rook,
    knight,
    bishop,
    king,
    queen
};

enum figures_color {
    white,
    black
};

enum Roles {
    Type = Qt::UserRole,
    Color,
    PositionX,
    PositionY
};

enum msg_type {
    NotSaved,
    SaveType,
    WrongHistory,
    MistakeInHistory
};

#endif // NEWTYPES_H
