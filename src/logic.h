#pragma once
#include <memory>
#include <QAbstractTableModel>
#include <QList>
#include "newtypes.h"
#include "implstruct.h"
#include "messages.h"
#include <QDebug>
#include <QString>
#include <QFileDialog>

class Logic: public QAbstractListModel
{
    Q_OBJECT
public:

    explicit Logic(QObject *parent = 0);
    ~Logic();

    Q_PROPERTY(int boardSize READ boardSize CONSTANT)
    Q_PROPERTY(bool playersTurn MEMBER whiteTurn NOTIFY changeOfStatus)
    Q_PROPERTY(QString moveString READ newMove NOTIFY newMoveMade)
    int boardSize() const;

	Q_INVOKABLE void startGame();
    Q_INVOKABLE void loadGame();

    Q_INVOKABLE void saveGame();
    Q_INVOKABLE bool endGame();
    Q_INVOKABLE void clear();

    Q_INVOKABLE bool move(const int fromX, const int fromY, const int toX, const int toY);

    Q_INVOKABLE void nextStep();
    Q_INVOKABLE void prevStep();

    Q_INVOKABLE QString newMove();

signals:
    void changeOfStatus();
    void newMoveMade();

protected:
    int rowCount(const QModelIndex & parent) const override;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;
    QHash<int, QByteArray> roleNames() const override;

private:
    std::unique_ptr<Impl>   impl;
    QList<QString>          history;
    Messages                msg;
    int                     historyIndex;
    bool                    whiteTurn;
    bool                    saved;
    bool                    load;

    QChar   figureName(const int target_cell);
    void    makeChanges(const int curr_figure, const int target_cell, int toX, int toY);
    bool    makeHistory(const int target_cell, const int fromX, const int fromY, const int toX, const int toY);
    void    restoreFigure(QChar figure_type, const int color, const int fromX, const int fromY);

};
