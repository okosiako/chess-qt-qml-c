#include "logic.h"

Logic::Logic(QObject *parent) : QAbstractListModel(parent), impl(new Impl())
{
}

Logic::~Logic()
{
}

int Logic::boardSize() const {
    return BOARD_SIZE;
}

//public functions only Q_INVOKABLE

void Logic::startGame()
{
    //pawns
    for (auto a = 0; a < BOARD_SIZE; a++)
    {
        impl->figures << Figure {pawn, white, a, 1};
        impl->figures << Figure {pawn, black, a, 6};
    }
    //figures on last and first lines
    for (auto r = 0, l = 7, f = 1; f != king; r++, l--, f++)
    {
        impl->figures << Figure {f, white, r, 0};
        impl->figures << Figure {f, white, l, 0};
        impl->figures << Figure {f, black, r, 7};
        impl->figures << Figure {f, black, l, 7};
    }
    impl->figures << Figure {king, white, 3, 0};
    impl->figures << Figure {queen, white, 4, 0};
    impl->figures << Figure {king, black, 3, 7};
    impl->figures << Figure {queen, black, 4, 7};
    whiteTurn = true;
    saved = false;
    load = false;
    historyIndex = 0;
}

void Logic::loadGame()
{
    /*
     * need "QFileDialog::DontUseNativeDialog" Option to avoid connection:error message
     */
    QString fileName = QFileDialog::getOpenFileName(0,
                                                    tr("Load history"), "", tr("Game History (*.gh)"), 0, QFileDialog::DontUseNativeDialog);

    if (fileName.isEmpty())
        return ;
    QFile file (fileName);
    if (file.exists() && file.open(QIODevice::ReadOnly))
    {
        QTextStream read(&file);
        while(!read.atEnd()) {
            QString line = read.readLine();
            if (!line.isEmpty())
                history.append(line);
        }
        load = true;
    }
    file.close();
}

void Logic::saveGame()
{
    msg.createMessage(SaveType);

    /*
     * need "QFileDialog::DontUseNativeDialog" Option to avoid connection:error message
     */
    QString fileName = QFileDialog::getSaveFileName(0,
                                                    tr("Save Game History"), "",
                                                    tr("Game History (*.gh)"), 0, QFileDialog::DontUseNativeDialog);
    if (fileName.isEmpty())
        return ;

    QFile file(fileName);
    if (file.open(QIODevice::ReadWrite | QIODevice::Truncate | QIODevice::Text))
    {
        QTextStream stream(&file);
        for (int i = 0; i < history.size(); ++i)
            stream << history[i] << endl;
        saved = true;
    }
    file.close();
}

bool Logic::endGame()
{
    if (!saved)
        if (!msg.createMessage(NotSaved))
            return false;
    clear();
    return true;
}

void Logic::clear() {
    beginResetModel();
    impl->figures.clear();
    endResetModel();
    history.clear();
}

bool Logic::move(const int fromX, const int fromY, const int toX, const int toY) {
    const int curr_figure = impl->findByPosition(fromX, fromY);
    const int target_cell = impl->findByPosition(toX, toY);

    if      ((whiteTurn && impl->figures[curr_figure].color == black) ||
             (!whiteTurn && impl->figures[curr_figure].color == white) ||
             !impl->canMakeMove(curr_figure, toX, toY))
        return false;

    if (makeHistory(target_cell, fromX, fromY, toX, toY))
    {
        makeChanges(curr_figure, target_cell, toX, toY);
        return true;
    }
    return false;
}

void Logic::nextStep()
{
    QString xalpha = "abcdefgh";
    if (historyIndex < history.size())
    {
        QString currMove = history[historyIndex];
        if (currMove.isEmpty())
        {
            msg.createMessage(WrongHistory);
            return ;
        }
        const int fromX = xalpha.indexOf(currMove[0]);
        const int fromY = currMove[1].digitValue() - 1;
        const int toX = xalpha.indexOf(currMove[2]);
        const int toY = currMove[3].digitValue() - 1;

        if (fromX < 0 || fromY < 0 || !move(fromX, fromY, toX, toY))
        {
            msg.createMessage(WrongHistory);
            return ;
        }
        historyIndex++;
    }
}

void Logic::prevStep()
{
    QString xalpha = "abcdefgh";
    if (historyIndex - 1 >= 0)
    {
        historyIndex--;
        QString currMove = history[historyIndex];
        if (currMove.isEmpty())
        {
            msg.createMessage(WrongHistory);
            return ;
        }
        const int fromX = xalpha.indexOf(currMove[2]);
        const int fromY = currMove[3].digitValue() - 1;
        const int toX = xalpha.indexOf(currMove[0]);
        const int toY = currMove[1].digitValue() - 1;

        const int curr_figure = impl->findByPosition(fromX, fromY);
        const int target_cell = impl->findByPosition(toX, toY);
        makeChanges(curr_figure, target_cell, toX, toY);
        if (currMove[4] == ':')
            restoreFigure(currMove[5], impl->figures[curr_figure].color, fromX, fromY);
    }
}

QString Logic::newMove()
{
    if (history.isEmpty())
        return "";
    return history.last();
}

//protected functions

int Logic::rowCount(const QModelIndex & ) const {
    return impl->figures.size();
}

QVariant Logic::data(const QModelIndex & modelIndex, int role) const {
    if (!modelIndex.isValid()) {
        return QVariant();
    }

    int index = static_cast<int>(modelIndex.row());

    if (index >= impl->figures.size() || index < 0) {
        return QVariant();
    }

    Figure & figure = impl->figures[index];

    switch (role) {
    case Roles::Type     : return figure.type;
    case Roles::Color    : return figure.color;
    case Roles::PositionX: return figure.x;
    case Roles::PositionY: return figure.y;
    }
    return QVariant();
}

QHash<int, QByteArray> Logic::roleNames() const { 
    QHash<int, QByteArray> names;
    names.insert(Roles::Type      , "type");
    names.insert(Roles::Color     , "color");
    names.insert(Roles::PositionX , "positionX");
    names.insert(Roles::PositionY , "positionY");
    return names;
}

//private functions

QChar Logic::figureName(const int target_cell)
{
    const int figure_type = impl->figures[target_cell].type;

    switch (figure_type) {
    case pawn   : return 'P';
    case rook   : return 'R';
    case knight : return 'N';
    case bishop : return 'B';
    case queen  : return 'Q';
    default     : return 'K';
    }
    return 'M';
}

void Logic::makeChanges(const int curr_figure, const int target_cell, const int toX, const int toY)
{
    impl->figures[curr_figure].x = toX;
    impl->figures[curr_figure].y = toY;
    QModelIndex topLeft = createIndex(curr_figure, 0);
    QModelIndex bottomRight = createIndex(curr_figure, 0);
    emit dataChanged(topLeft, bottomRight);
    whiteTurn = !whiteTurn;
    emit changeOfStatus();
    if (target_cell != -1)
    {
        beginResetModel();
        impl->figures.removeAt(target_cell);
        endResetModel();
    }
}

bool Logic::makeHistory(const int target_cell, const int fromX, const int fromY, const int toX, const int toY)
{
    const QString xalpha = "abcdefgh";
    QString curr_move;
    QTextStream(&curr_move) << xalpha[fromX] << fromY + 1 << xalpha[toX] << toY + 1;
    target_cell != -1 ? curr_move += ':' + figureName(target_cell) : 0;

    if (load)
    {
        if (curr_move != history[historyIndex])
            return (msg.createMessage(MistakeInHistory));
    }
    else
    {
        history << curr_move;
        emit newMoveMade();
    }
    return true;
}

void Logic::restoreFigure(QChar figure_type, const int color, const int fromX, const int fromY)
{
    const QString temp = "PRNBKQ";
    const int type = temp.indexOf(figure_type);
    const int _color = color == white ? black : white;
    beginResetModel();
    impl->figures << Figure {type, _color, fromX, fromY};
    endResetModel();
}
