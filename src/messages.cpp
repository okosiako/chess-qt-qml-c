#include "messages.h"

Messages::Messages()
{
}

Messages::~Messages()
{
}

bool Messages::createMessage(const int type)
{
    if (type == NotSaved)
    {
        QMessageBox msgBox;
        msgBox.setText("The Game was not saved.");
        msgBox.setInformativeText("Are you sure you want end this game?");
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.setDefaultButton(QMessageBox::No);
        msgBox.setFixedSize(500,200);
        int ret = msgBox.exec();
        if (ret == QMessageBox::No)
            return false;
    }
    else if (type == SaveType)
    {
        QMessageBox messageBox;
        messageBox.information(0, "Note", "You have to add \".gh\" to the end of a filename");
        messageBox.setFixedSize(500, 200);

    }
    else if (type == WrongHistory)
    {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","It seems like the history of game was changed");
        messageBox.setFixedSize(500,200);
        return false;
    }
    else if (type == MistakeInHistory)
    {
        QMessageBox messageBox;
        messageBox.critical(0,"Error","Loaded history contains a mistake");
        messageBox.setFixedSize(500,200);
        return false;
    }
    return true;
}
