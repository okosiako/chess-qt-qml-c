TEMPLATE = app

CONFIG += c++11
CONFIG += CONSOLE

QT += qml quick widgets core

SOURCES +=  src/main.cpp \
    src/implstruct.cpp \
    src/messages.cpp \
    src/logic.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

HEADERS += \
    src/newtypes.h \
    src/implstruct.h \
    src/messages.h \
    src/logic.h
